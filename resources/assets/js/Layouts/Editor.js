import React, { Component } from 'react';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'font-awesome/css/font-awesome.css';
import FroalaEditor from 'react-froala-wysiwyg';
import './Layout.css';
import './vi';
// import './image.min.js';
class EditorComponent extends Component {
    //   constructor () {
    //     super();

    //     this.handleModelChange = this.handleModelChange.bind(this);

    //     this.state = {
    //       model: 'Example text'
    //     };
    //   }

    //   handleModelChange(model) {
    //     this.setState({
    //       model: model
    //     });
    //   }

    componentDidMount() {
        const {token} = localStorage.getItem('@token');
        $('#editor').froalaEditor({
            imageUploadURL: '/api/upload',
            videoUploadURL: '/api/upload',
            fileUploadURL: '/api/upload',
            imageManagerLoadURL: '/api/get-image?token=' + token,
            imageManagerDeleteURL: 'api/delete-image',
            language: 'vi'
        });

    }
    render() {
        return (
            <div id='editor'></div>
        );
    }
}
export default EditorComponent;
