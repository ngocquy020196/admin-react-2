import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import ArrowDropDown from '@material-ui/icons/ThumbsUpDown';
class SidebarItem extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      isOpen: false
    });
  }
  componentDidMount() {
    if (document.getElementsByClassName('li-active').length != 0) {
      var active = document.getElementById('submenu');
      active.classList.add('submenu-active');
    }
  }

  clickopen() {
    var el = document.getElementsByClassName('submenu-active');
    if (document.getElementsByClassName('submenu-active').length === 0) {
      submenu.classList.add('submenu-active');
    } else {
      submenu.classList.remove('submenu-active');
    }
  }
  render() {
    return (
      <div>
        <ul className="mainmenu">
          <li>
            <NavLink to='/xjk-system'>Dashboard</NavLink>
          </li>
          <li>
            <NavLink to='#' onClick={this.clickopen.bind(this)}>Cấu hình</NavLink>
            <ul id="submenu">
              <li>
                <NavLink exact to='/xjk-system/company' activeClassName="li-active">Thông tin công ty</NavLink>
              </li>
              <li>
                <NavLink exact to='/xjk-system/social' activeClassName="li-active">Mạng xã hội</NavLink>
              </li>
              <li>
                <NavLink exact to='/xjk-system/seo' activeClassName="li-active">Thông tin SEO</NavLink>
              </li>
              <li>
                <NavLink exact to='/xjk-system/logo' activeClassName="li-active">Logo</NavLink>
              </li>
              <li>
                <NavLink exact to='/xjk-system/plugin' activeClassName="li-active">Plugin</NavLink>
              </li>
              <li>
                <NavLink exact to='/xjk-system/map' activeClassName="li-active">Map</NavLink>
              </li>
            </ul>
          </li>
          <li>
            <NavLink to='/xjk-system/category'>Danh mục</NavLink>
          </li>
          <li>
            <NavLink to='/xjk-system/post'>Bài viết</NavLink>
          </li>
          <li>
            <NavLink to='/xjk-system/seo'>Sản phẩm</NavLink>
          </li>
          <li>
            <NavLink to='/xjk-system/seo'>Media</NavLink>
          </li>
          <li>
            <NavLink to='/xjk-system/seo'>Đơn hàng</NavLink>
          </li>
          <li>
            <NavLink to='/xjk-system/seo'>Tin đăng</NavLink>
          </li>
          <li>
            <NavLink to='/xjk-system/seo'>Nhân viên</NavLink>
          </li>
          <li>
            <NavLink to='/xjk-system/seo'>Thành viên</NavLink>
          </li>
        </ul>
      </div>
    );
  }
}

export default SidebarItem;

