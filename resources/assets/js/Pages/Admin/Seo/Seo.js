import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { Helmet } from "react-helmet";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '30%',
    },
    Header: {
        width: '100%',
        textAlign: 'center',
    },
    button: {
        margin: theme.spacing.unit,
    },
});


class Seo extends Component {
    constructor(props){
        super(props);
        this.state =({
            MetaTitle:'',
            MetaDescription:'',
            MetaKeyword:''
        })
    }
    async componentDidMount(){
        await this.props.getCompany();
        const { Company } = this.props;
        this.setState({
            MetaTitle:Company.MetaTitle,
            MetaDescription:Company.MetaDescription,
            MetaKeyword:Company.MetaKeyword,
        })
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.container}>
                <Helmet>
                    <title>Thông tin SEO</title>
                </Helmet>
                <Typography variant="headline" className={classes.Header} gutterBottom>
                    Thông tin SEO
            </Typography>
                <TextField
                    label="Meta title"
                    id="facebook"
                    value={this.state.MetaTitle}
                    className={classes.textField}
                // helperText="Some important text"
                />
                <TextField
                    label="Meta description"
                    id="facebook"
                    value={this.state.MetaDescription}
                    className={classes.textField}
                // helperText="Some important text"
                />
                <TextField
                    label="Meta keyword"
                    id="facebook"
                    value={this.state.MetaKeyword}
                    className={classes.textField}
                // helperText="Some important text"
                />

                <Button variant="raised" size="medium" color="primary" className={classes.button}>
                    Lưu
            </Button>
            </div>
        );
    }
}
Seo.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Seo);
