import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { Helmet } from "react-helmet";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '45%',
    },
    Header: {
        width: '100%',
        textAlign: 'center',
    },
    button: {
        margin: theme.spacing.unit,
    },
});

const Logo = props => {
    const { classes } = props;

    return (
        <div className={classes.container}>
            <Helmet>
                <title>Logo</title>
            </Helmet>
            <h1>Logo</h1>
            <Button variant="raised" size="medium" color="primary" className={classes.button}>
                Lưu
            </Button>
        </div>
    );
};

Logo.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Logo);
