import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { Helmet } from "react-helmet";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '45%',
    },
    Header: {
        width: '100%',
        textAlign: 'center',
    },
    button: {
        margin: theme.spacing.unit,
    },
});


class Social extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            Facebook: '',
            Twitter: '',
            GooglePlus: '',
            Zalo: '',
            Skype: '',
            Viber: '',
            ErrorFacebook: '',
            ErrorTwitter: '',
            ErrorGooglePlus: '',
            ErrorZalo: '',
            ErrorSkype: '',
            ErrorViber: '',
        });
    }

    async componentDidMount() {
        await this.props.getCompany();
        const { Company } = this.props;
        this.setState({
            Facebook: Company.Facebook,
            Twitter: Company.Twitter,
            GooglePlus: Company.GooglePlus,
            Zalo: Company.Zalo,
            Skype: Company.Skype,
            Viber: Company.Viber
        })
    }

    onChange(event) {
        var target = event.target;
        var name = target.name;
        var errorName = `error${name}`;
        var value = target.value;
        this.setState({
            [name]: value,
            [errorName]: null
        });
    }

    async onSubmit(event) {
        event.preventDefault();
        const { Facebook, Twitter, GooglePlus, Zalo, Skype, Viber } = this.state;
        await this.props.UpdateSocial(Facebook,GooglePlus,0,Twitter,Zalo,Viber,Skype);
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.container}>
                <Helmet>
                    <title>Mạng xã hội</title>
                </Helmet>
                <Typography variant="headline" className={classes.Header} gutterBottom>
                    Mạng xã hội
            </Typography>
                <TextField
                    label="Facebook"
                    id="facebook"
                    value={this.state.Facebook}
                    className={classes.textField}
                    onChange={this.onChange.bind(this)}
                    name="Facebook"
                // helperText="Some important text"
                />
                <TextField
                    label="Twitter"
                    id="twitter"
                    value={this.state.Twitter}
                    className={classes.textField}
                    onChange={this.onChange.bind(this)}
                    name="Twitter"
                // helperText="Some important text"
                />
                <TextField
                    label="Google plus"
                    id="google-plus"
                    value={this.state.GooglePlus}
                    className={classes.textField}
                    onChange={this.onChange.bind(this)}
                    name="GooglePlus"
                // helperText="Some important text"
                />
                <TextField
                    label="Zalo"
                    id="zalo"
                    value={this.state.Zalo}
                    className={classes.textField}
                    onChange={this.onChange.bind(this)}
                    name="Zalo"
                // helperText="Some important text"
                />
                <TextField
                    label="Skype"
                    id="skype"
                    value={this.state.Skype}
                    className={classes.textField}
                    onChange={this.onChange.bind(this)}
                    name="Skype"
                // helperText="Some important text"
                />
                <TextField
                    label="Viber"
                    id="viber"
                    value={this.state.Viber}
                    className={classes.textField}
                    onChange={this.onChange.bind(this)}
                    name="Viber"
                // helperText="Some important text"
                />

                <Button variant="raised" size="medium" color="primary" onClick={this.onSubmit.bind(this)} className={classes.button}>
                    Lưu
            </Button>
            </div>
        );
    }
}
Social.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Social);
