
import React, { Component } from 'react';

import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { get_token } from './../../actions/Login';

import Loadable from 'react-loadable';
import { LinearProgress } from 'material-ui/Progress';
const Loading = () => <div><LinearProgress /></div>;
const DashboardContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Dashboard/DashboardContainer'),
	loading: Loading,
});
const CompanyContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Company/CompanyContainer'),
	loading: Loading,
});

const SocialContaier = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Social/SocialContainer'),
	loading: Loading,
});

const SeoContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Seo/SeoContainer'),
	loading: Loading,
});

const LogoContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Logo/LogoContainer'),
	loading: Loading,
});

const PluginContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Plugin/PluginContainer'),
	loading: Loading,
});

const CategoryContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Category/CategoryContainer'),
	loading: Loading,
});

const PostContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Post/PostContainer'),
	loading: Loading,
});

const MapContainer = Loadable({
	loader: () =>
		System.import('./../../container/Admin/Map/MapContainer'),
	loading: Loading,
});




class RouteAdmin extends Component {
	// constructor(props) {
	// 	super(props);
	// 	this.state = ({
	// 		isLoadding: false
	// 	})
	// }
	render() {
		const { isLogin } = this.props;
		console.log('kiểm tra url',this.props);
		console.log('checkurrl',`${this.props.match.url}/company`);
		return (
			<Switch>
				<Route exact path={`${this.props.match.path}`} component={DashboardContainer} />
				<Route path={`${this.props.match.path}/company`} component={CompanyContainer} />
				<Route path={`${this.props.match.path}/social`} component={SocialContaier} />
				<Route path={`${this.props.match.path}/logo`} component={LogoContainer} />
				<Route path={`${this.props.match.path}/seo`} component={SeoContainer} />
				<Route path={`${this.props.match.path}/plugin`} component={PluginContainer} />
				<Route path={`${this.props.match.path}/category`} component={CategoryContainer} />
				<Route path={`${this.props.match.path}/post`} component={PostContainer} />
				<Route path={`${this.props.match.path}/map`} component={MapContainer} />
			</Switch>
		);
	}
}
function mapStateToProps(state) {
	return {
		isLogin: state.Login.isLogin
	};
}
function mapDispatchToProps(dispatch) {
	return {
		checkToken: () => dispatch(get_token())
	};
}
export default connect(null, null)(RouteAdmin)