import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { Helmet } from "react-helmet";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '45%',
    },
    Header: {
        width: '100%',
        textAlign: 'center',
    },
    button: {
        margin: theme.spacing.unit,
    },
});


class Company extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            Name: '',
            Email: '',
            Hotline: '',
            Address: '',
            ErrorName: '',
            ErrorEmail: '',
            ErrorHotline: '',
            ErrorAddress: '',
        })
    }
    async componentDidMount() {
        await this.props.getCompany();
        const { Company } = this.props;
        this.setState({
            Name: Company.Name,
            Email: Company.Email,
            Hotline: Company.Hotline,
            Address: Company.Address
        })
    }

    async clickSubmit(even) {
        event.preventDefault();
        var { Name, Email, Address, Hotline } = this.state;
        if (Name === '') {
            this.setState({ ErrorName: 'Vui lòng nhập tên công ty.' });
        } else if (Email === '') {
            this.setState({ ErrorEmail: 'Vui lòng nhập email.' });
        } else if (Hotline === '') {
            this.setState({ ErrorHotline: 'Vui lòng nhập hotline.' });
        } else if (Address === '') {
            this.setState({ ErrorAddress: 'Vui lòng nhập địa chỉ.' });
        } else {
            await this.props.UpdateInfo(Email, Name, Hotline, Address);
            const { UpdateInfo } = this.props;
        }
        // const {UpdateInfo} = this.props;
        console.log(this.props)
    }

    onChange(event) {
        var target = event.target;
        var name = target.name;
        var errorName = `error${name}`;
        var value = target.value;
        this.setState({
            [name]: value,
            [errorName]: null
        });
    }

    render() {
        const { classes } = this.props;
        const { Name, Email, Address, Hotline } = this.state;
        // console.log('check state', Name);
        return (
            <div className={classes.container}>
                <Helmet>
                    <title>Thông tin công ty</title>
                </Helmet>
                <Typography variant="headline" className={classes.Header} gutterBottom>
                    Thông tin công ty
            </Typography>
                <TextField
                    label="Tên công ty"
                    id="name-company"
                    onChange={this.onChange.bind(this)}
                    value={Name}
                    className={classes.textField}
                    name='Name'
                    helperText={this.state.ErrorName}
                />
                <TextField
                    label="Email công ty"
                    id="email-company"
                    onChange={this.onChange.bind(this)}
                    value={Email}
                    className={classes.textField}
                    name='Email'
                    helperText={this.state.ErrorEmail}
                />
                <TextField
                    label="Hotline"
                    id="hotline-company"
                    onChange={this.onChange.bind(this)}
                    value={Hotline}
                    className={classes.textField}
                    name='Hotline'
                    helperText={this.state.ErrorHotline}
                />
                <TextField
                    label="Địa chỉ"
                    id="address-company"
                    onChange={this.onChange.bind(this)}
                    value={Address}
                    className={classes.textField}
                    name='Address'
                    helperText={this.state.ErrorAddress}
                    // error={(typeof(this.state.ErrorAddress) === null) ? false : true}
                />

                <Button variant="raised" size="medium" color="primary" onClick={this.clickSubmit.bind(this)} className={classes.button}>
                    Lưu
            </Button>
            </div>
        )
    }
}

Company.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Company);
