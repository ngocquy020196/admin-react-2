import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import { Link } from 'react-router-dom';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
    title: {
        textAlign: 'center'
    },
    th: {
        borderRadius: 0,
    },
    button:{
        float:'right',
        margin:'10px'
    }
});

let id = 0;
function createData(name, description, slug) {
    id += 1;
    return { id, name, description, slug };
}

const data = [
    createData('Danh mục 1', 'Mô tả ', 'Đường dẫn'),
    createData('Danh mục 2', 'Mô tả ', 'Đường dẫn'),
    createData('Danh mục 3', 'Mô tả ', 'Đường dẫn'),
    createData('Danh mục 4', 'Mô tả ', 'Đường dẫn'),
    createData('Danh mục 5', 'Mô tả ', 'Đường dẫn')
];

function Category(props) {
    const { classes } = props;
    return (
        <Paper className={classes.root}>
            <Typography variant="display2" gutterBottom className={classes.title}>
                Danh mục
            </Typography>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <CustomTableCell className={classes.th}>STT</CustomTableCell>
                        <CustomTableCell className={classes.th}>Tên</CustomTableCell>
                        <CustomTableCell className={classes.th}>Mô tả</CustomTableCell>
                        <CustomTableCell className={classes.th}>Đường dẫn</CustomTableCell>
                        <CustomTableCell className={classes.th}>Hành động</CustomTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map(n => {
                        return (
                            <TableRow className={classes.row} key={n.id}>
                                <CustomTableCell className={classes.th123} component="th" scope="row">
                                    {n.id}
                                </CustomTableCell>
                                <CustomTableCell>{n.name}</CustomTableCell>
                                <CustomTableCell>{n.description}</CustomTableCell>
                                <CustomTableCell>{n.slug}</CustomTableCell>
                                <CustomTableCell>
                                    <Link to='/'>Xóa</Link>
                                </CustomTableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
            <Button variant="raised" size="small" color="primary" className={classes.button}>
                Thêm danh mục
            </Button>
        </Paper>
    );
}

Category.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Category);
