import React, { Component } from 'react';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import { Helmet } from "react-helmet";

class Dashboard extends Component {
    render() {
        const { classes } = this.props;

        return (
            <Grid container spacing={24}>
                {/* <Sidebar />	                     */}
                <Helmet>
                    <title>Trang tổng quan</title>
                </Helmet>
                <Grid item xs={4}>
                    <Card className='card'>
                        <CardContent>
                            <Typography component='h1'>
                                Người dùng
                            </Typography>
                            <Typography color="textSecondary" component='h1'>
                                30
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={4}>
                    <Card className='card'>
                        <CardContent>
                            <Typography component='h1'>
                                Người dùng
                            </Typography>
                            <Typography color="textSecondary" component='h1'>
                                30
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={4}>
                    <Card className='card'>
                        <CardContent>
                            <Typography component='h1'>
                                Người dùng
                            </Typography>
                            <Typography color="textSecondary" component='h1'>
                                30
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={4}>
                    <Card className='card'>
                        <CardContent>
                            <Typography component='h1'>
                                Người dùng
                            </Typography>
                            <Typography color="textSecondary" component='h1'>
                                30
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        );
    }
}

export default Dashboard;