import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { Helmet } from "react-helmet";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '45%',
    },
    Header: {
        width: '100%',
        textAlign: 'center',
    },
    button: {
        margin: theme.spacing.unit,
    },
});


class PluginWeb extends Component {
    constructor(props){
        super(props);
        this.state = ({
            Analytic:'',
            ChatBox:''
        })
    }

    async componentDidMount(){
        await this.props.getCompany();
        const { Company } = this.props;
        this.setState({
            Analytic:Company.Analytic,
            ChatBox:Company.ChatBox,
        })
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.container}>
                <Helmet>
                    <title>Plugin</title>
                </Helmet>
                <Typography variant="headline" className={classes.Header} gutterBottom>
                    Plugin
            </Typography>

                <TextField
                    id="google"
                    label="Mã Google Analytic"
                    multiline
                    rows="4"
                    className={classes.textField}
                    margin="normal"
                    value={this.state.Analytic}
                />
                <TextField
                    id="google"
                    label="Mã Chatbox"
                    multiline
                    rows="4"
                    className={classes.textField}
                    margin="normal"
                    value={this.state.ChatBox}
                />
                <Button variant="raised" size="medium" color="primary" className={classes.button}>
                    Lưu
            </Button>
            </div>
        );
    }
}

PluginWeb.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PluginWeb);
