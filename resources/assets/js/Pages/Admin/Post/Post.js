import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import { Link } from 'react-router-dom';
import Typography from 'material-ui/Typography';
import Editor from '../../../Layouts/Editor';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import TextField from 'material-ui/TextField';

const styles = theme => ({
    tabs: {
        color: 'white'
    }
});

const theme = () => {
    direction: 'rtl'
};
function TabContainer({ children }) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

class Post extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            value: 0,
        });
    }
    handleChange(event, value) {
        this.setState({ value });
    };

    render() {
        return (
            <Paper>
                <div>
                    <Typography variant="display2" gutterBottom>
                        Bài viết
                    </Typography>
                    <AppBar position="static" color="default">
                        <Tabs
                            value={this.state.value}
                            onChange={this.handleChange.bind(this)}
                            indicatorColor="primary"
                            textColor="primary"
                            fullWidth
                        >
                            <Tab label="Nội dung bài viết" />
                            <Tab label="SEO" />
                            <Tab label="Hình ảnh" />
                        </Tabs>
                    </AppBar>
                    <SwipeableViews
                        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                        index={this.state.value}
                    >
                        <TabContainer>
                            <TextField
                                label="Tiêu đề bài viết"
                                fullWidth
                                id="title"
                            // defaultValue="Default Value"
                            // helperText="Some important text"
                            />
                            <Editor />
                        </TabContainer>
                        <TabContainer>
                            <TextField
                                label="Meta title"
                                fullWidth
                                id="meta-title"
                            // defaultValue="Default Value"
                            // helperText="Some important text"
                            />
                            <TextField
                                label="Meta description"
                                fullWidth
                                id="meta-description"
                            // defaultValue="Default Value"
                            // helperText="Some important text"
                            />
                        </TabContainer>
                        <TabContainer>
                            s
                        </TabContainer>
                    </SwipeableViews>

                </div>
            </Paper>
        );
    }
}
Post.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Post);

