import { API_UPDATE_SOCIAL } from './../constants/Config';

export default (Facebook,GooglePlus,Youtube,Twitter,Zalo,Viber,Skype) => axios({
    method: 'POST',
    url: API_UPDATE_SOCIAL,
    data:{Facebook,GooglePlus,Youtube,Twitter,Zalo,Viber,Skype,token:localStorage.getItem('@token')}
}).then(Respon => Respon.data)
    // .catch(e => e.response.data)
