import { API_COMPANY } from './../constants/Config';

export default () => axios({
    method: 'GET',
    url: API_COMPANY+'?token=' +localStorage.getItem('@token') ,
}).then(Respon => Respon.data)
    // .catch(e => e.response.data)
