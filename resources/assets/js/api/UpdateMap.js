import { API_UPDATE_MAP } from './../constants/Config';

export default (Lat,Lng) => axios({
    method: 'POST',
    url: API_UPDATE_MAP,
    data:{Lat,Lng,token:localStorage.getItem('@token')}
}).then(Respon => Respon.data)
    // .catch(e => e.response.data)
