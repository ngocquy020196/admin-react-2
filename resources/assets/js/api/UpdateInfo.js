import { API_UPDATE_INFO } from './../constants/Config';

export default (Email,Name,Hotline,Address,Phone,TaxCode,Website) => axios({
    method: 'POST',
    url: API_UPDATE_INFO,
    data:{Email,Name,Hotline,Address,Phone,TaxCode,Website,token:localStorage.getItem('@token')}
}).then(Respon => Respon.data)
    // .catch(e => e.response.data)
