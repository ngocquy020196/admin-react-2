import * as Types from './../constants/Company';
import Company from './../api/Company';

export const get_company = () => dispath =>
    Company()
        .then(data => { return dispath(company_success(data)) })
        .catch(e => { return dispath(company_fail(e.response.data)) });
export const company_success = data => {
    return {
        type: Types.COMPANY_SUCCESS,
        data
    }
}

export const company_fail = data => {
    return {
        type: Types.COMPANY_FAIL,
        data
    }
}
