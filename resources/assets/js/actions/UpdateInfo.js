import * as Types from './../constants/UpdateInfo';
import UpdateInfo from './../api/UpdateInfo';

export const update_info = (Email,Name,Hotline,Address,Phone,TaxCode,Website) => dispath =>
UpdateInfo(Email,Name,Hotline,Address,Phone,TaxCode,Website)
        .then(data => { return dispath(update_success(data)) })
        .catch(e => { return dispath(update_fail(e.response.data)) });
export const update_success = data => {
    return {
        type: Types.UPDATE_SUCCESS,
        data
    }
}

export const update_fail = data => {
    return {
        type: Types.UPDATE_FAIL,
        data
    }
}
