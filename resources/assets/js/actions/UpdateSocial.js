import * as Types from './../constants/UpdateSocial';
import UpdateSocial from './../api/UpdateSocial';

export const update_social = (Facebook,GooglePlus,Youtube,Twitter,Zalo,Viber,Skype) => dispath =>
UpdateSocial(Facebook,GooglePlus,Youtube,Twitter,Zalo,Viber,Skype)
        .then(data => { return dispath(update_success(data)) })
        .catch(e => { return dispath(update_fail(e.response.data)) });
export const update_success = data => {
    return {
        type: Types.UPDATE_SUCCESS,
        data
    }
}

export const update_fail = data => {
    return {
        type: Types.UPDATE_FAIL,
        data
    }
}
