import * as Types from './../constants/UpdateMap';
import UpdateMap from './../api/UpdateMap';

export const update_map = (Lat,Lng) => dispath =>
UpdateMap(Lat,Lng)
        .then(data => { return dispath(update_success(data)) })
        .catch(e => { return dispath(update_fail(e.response.data)) });
export const update_success = data => {
    return {
        type: Types.UPDATE_SUCCESS,
        data
    }
}

export const update_fail = data => {
    return {
        type: Types.UPDATE_FAIL,
        data
    }
}
