import { combineReducers } from 'redux';
import Login from './Login';
import Company from './Company';
import UpdateInfo from './UpdateInfo';
import UpdateSocial from './UpdateSocial';
import UpdateMap from './UpdateMap';
const myReducer = combineReducers({
    Login,
    Company,
    UpdateInfo,
    UpdateMap

});

export default myReducer;