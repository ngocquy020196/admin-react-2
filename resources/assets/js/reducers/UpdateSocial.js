import * as types from './../constants/UpdateSocial';
var initialState = {
    Msg:null
};

var myReducers = (state = initialState, action) => {
    const { data } = action;
    switch (action.type) {
        case types.UPDATE_SUCCESS:
            return {
                ...state,
                Msg: data.msg
            };

        case types.UPDATE_FAIL:
            return {
                ...state,
                Msg: data.msg
            };
        default: return state;
    }

}
export default myReducers;