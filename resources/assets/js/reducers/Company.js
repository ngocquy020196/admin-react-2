import * as types from './../constants/Company';
var initialState = {
    Company:null,
    Msg:null
};

var myReducers = (state = initialState, action) => {
    const { data } = action;
    switch (action.type) {
        case types.COMPANY_SUCCESS:
            return {
                ...state,
                Company:data.item,
                Msg: data.msg
            };

        case types.COMPANY_FAIL:
            return {
                ...state,
                Company:null,
                Msg: data.msg
            };
        default: return state;
    }

}
export default myReducers;