import React from 'react';
import { connect } from 'react-redux';
// import { send_login } from './../../../actions/Login'
import Social from './../../../Pages/Admin/Social/Social';
import { update_social } from './../../../actions/UpdateSocial';
import { get_company } from './../../../actions/Company';

function mapStateToProps(state) {
    return {
        Company:state.Company.Company,
        Msg:state.Company.Msg
    };
}
function mapDispatchToProps(dispatch) {
    return {
        getCompany: () => dispatch(get_company()),
        UpdateSocial:(Facebook,GooglePlus,Youtube,Twitter,Zalo,Viber,Skype)=>dispatch(update_social(Facebook,GooglePlus,Youtube,Twitter,Zalo,Viber,Skype))
    };
}
// connect(null, null, null, { pure: false })
// export default Social;
export default connect(mapStateToProps, mapDispatchToProps)(Social);
