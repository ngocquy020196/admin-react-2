import React from 'react';
import { connect } from 'react-redux';
import { update_map } from './../../../actions/UpdateMap'
import { get_company } from './../../../actions/Company'

import Map from './../../../Pages/Admin/Map/Map';

function mapStateToProps(state) {
    return {
        Company: state.Company.Company,
        Msg: state.UpdateMap.Msg,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        getCompany: () => dispatch(get_company()),
        UpdateMap: (Lat, Lng) => dispatch(update_map(Lat, Lng))
    };
}
// connect(null, null, null, { pure: false })
// export default Map;
export default connect(mapStateToProps, mapDispatchToProps)(Map);