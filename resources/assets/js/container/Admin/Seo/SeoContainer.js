import React from 'react';
import { connect } from 'react-redux';
// import { send_login } from './../../../actions/Login'
import { get_company } from './../../../actions/Company';
import Seo from './../../../Pages/Admin/Seo/Seo';

function mapStateToProps(state) {
    return {
        Company:state.Company.Company,
        Msg:state.Company.Msg
    };
}
function mapDispatchToProps(dispatch) {
    return {
        getCompany: () => dispatch(get_company())
    };
}
// connect(null, null, null, { pure: false })
// export default Seo;
export default connect(mapStateToProps, mapDispatchToProps)(Seo);
