import React from 'react';
import { connect } from 'react-redux';
import { get_company } from './../../../actions/Company';
import { update_info } from './../../../actions/UpdateInfo';

import Company from './../../../Pages/Admin/Company/Company';

function mapStateToProps(state) {
    return {
        Company:state.Company.Company,
        Msg:state.Company.Msg
    };
}
function mapDispatchToProps(dispatch) {
    return {
        getCompany: () => dispatch(get_company()),
        UpdateInfo: (Email,Name,Hotline,Address,Phone,TaxCode,Website) => dispatch(update_info(Email,Name,Hotline,Address,Phone,TaxCode,Website))
    };
}
// connect(null, null, null, { pure: false })
// export default Company;
export default connect(mapStateToProps, mapDispatchToProps,null,{ pure: false })(Company);
