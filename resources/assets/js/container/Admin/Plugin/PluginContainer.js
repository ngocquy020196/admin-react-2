import React from 'react';
import { connect } from 'react-redux';
// import { send_login } from './../../../actions/Login'
import { get_company } from './../../../actions/Company';
import PluginWeb from './../../../Pages/Admin/Plugin/PluginWeb';

function mapStateToProps(state) {
    return {
        Company:state.Company.Company,
        Msg:state.Company.Msg
    };
}
function mapDispatchToProps(dispatch) {
    return {
        getCompany: () => dispatch(get_company())
    };
}
// connect(null, null, null, { pure: false })
// export default PluginWeb;
export default connect(mapStateToProps, mapDispatchToProps)(PluginWeb);