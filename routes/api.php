<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user/login','UserController@postLogin');
Route::post('checkToken', 'UserController@postCheckToken');

Route::group(['middleware' => ['jwt.auth']], function () {
    Route::post('upload','UploadController@ApiUpload');
    Route::post('delete-image','UploadController@DeleteImage');
    Route::get('get-image','UploadController@GetImage');
    Route::post('user/add', 'UserController@postAddUser');
    Route::group(['prefix' => 'company'], function() {
        Route::get('/','CompanyController@index');
        Route::get('/{id}','CompanyController@edit')->where(['id'=>'[0-9]+']);
        Route::post('/info','CompanyController@updateInfo');
        Route::post('/social','CompanyController@updateSocial');
        Route::post('/map','CompanyController@updateMap');
    });
});
