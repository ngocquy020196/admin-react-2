<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File,Storage;

class UploadController extends Controller
{
    public function ApiUpload(Request $request){
        if($request->file('file')){
            $file = $request->file('file');
            $type = $file->getClientOriginalExtension();
            $name = str_slug(time() .'-'.$file->getClientOriginalName());
            $name = str_slug(str_replace($type,'',$name));
            $name = $name.'.'.$type;
            $path = 'upload/';
            $file->move($path,$name);
            return response()->json(['link'=> asset($path.$name)]);
        }
    }

    public function GetImage(){
        $data = [];
        $files = glob('upload/*.{jpg,png,gif,bmp,tiff,jpeg}',GLOB_BRACE);
        foreach ($files as $file) {
            array_push($data,["url" => asset($file),'thumb'=> asset($file),'tag'=> 'image']);
        }
        return response()->json($data);
    }

    public function DeleteImage(Request $request){
        $path = str_replace(asset('upload'),'',$request->src);
        Storage::disk('upload')->delete($path);
        return response()->json($request->src);
    }
}
